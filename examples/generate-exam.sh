#!/bin/sh

if [ ! -f ./../gen ]; then
    echo "ERROR: Executable 'gen' file not found."
    echo "HINT: Go to parent directory and execute \`make gen\`."
    exit 1
elif [ ! -f ./../gen ]; then
    echo "ERROR: File 'gen' does not have execution permission."
    echo "HINT: Go to parent directory and execute \`chmod +x gen\`."
    exit 1
fi


#
# Execute question raffle
#
./../gen -n 1 -i ./dummy-exam/exbda.tex -o exbda.tex
./../gen -n 1 -i ./dummy-exam/exlau.tex -o exlau.tex
./../gen -n 1 -i ./dummy-exam/exmat.tex -o exmat.tex
cp ./dummy-exam/root.tex .

#
# Execute LaTeX compiler
#
if [ -x pdflatex ]; then
	# latexmk -cd -f -pdf -interaction=nonstopmode -synctex=1 root.tex
	pdflatex -interaction=nonstopmode -synctex=1 root.tex
else
    echo "WARNING: Compiler \`pdflatex\` not found."
    echo "HINT: Be sure that LaTeX compiler is installed."
    exit 1
fi
