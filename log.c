#include "log.h"
#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>

bool _verbose = false;

void setVerbose(bool setting) {
  _verbose = setting;
}

int verbose(const char* restrict format, ...) {
  if (!_verbose) {
      return 0;
  }

  va_list args;
  va_start(args, format);
  int ret = vfprintf(stderr, format, args);
  va_end(args);

  return ret;
}