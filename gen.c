#include "gen.h"
#include "log.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

int main (int argc, char **argv) {

  //
  // Initializing variables
  //
  bool    iflag = false;  // Bit of -i flag presence
  bool    oflag = false;  // Bit of -o flag presence
  bool    vflag = false;  // Bit of -v flag presence
  bool    nflag = false;  // Bit of -n flag presence
  char*   ivalue = NULL;  // Value of -i flag
  char*   ovalue = NULL;  // Value of -o flag
  uint8_t nvalue = 0;     // Value of -n flag
  int     c;
  FILE* fin;              // Pointer to input file
  FILE* fout;             // Pointer to output file
  long fsize;             // Input file size (in bytes)
  char* buffer;           // Buffer for input file content
  char* buf2count;        // Buffer for questions counting
  int ocur_count = 0;     // Question counter
  // int start;
  // int end;
  char** qst_ptrs;        // Questions temporal buffer
  bool* qst_raffle;       // Questions raffle flags

  opterr = 0;



  //
  // Command-line arguments reading
  //
  while ((c = getopt(argc, argv, "n:i:o:v")) != -1) {
    switch (c) {
      case 'i':
        iflag = true;
        ivalue = optarg;
        break;

      case 'o':
        oflag = true;
        ovalue = optarg;
        break;

      case 'v':
        setVerbose(vflag = true);
        break;

      case 'n':
        nflag = true;
        nvalue = atoi(optarg);
        break;

      case '?':
        if (optopt == 'c')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;

      default:
        abort ();
      }
  }



  //
  // Command-line argument validations
  //
  if (!iflag) {
    fprintf (stderr, "ERROR: Flag -i must be set.\n");
    return 1;
  }

  if (!oflag) {
    fprintf (stderr, "ERROR: Flag -o must be set.\n");
    return 1;
  }

  if (!nflag) {
    fprintf (stderr, "ERROR: Flag -n must be set.\n");
    return 1;
  }

  //
  // Output command-line argument if verbose
  //
  verbose("{n=\"%hhu\", i=\"%s\", o=\"%s\", v=%d}\n", nvalue, ivalue, ovalue, vflag);



  //
  // Read input file and store into a buffer
  //
  verbose("Opening input file...\n");

  // Open an input stream reader
  fin = fopen(ivalue, "r");
  fseek(fin, 0, SEEK_END);
  fsize = ftell(fin);
  fseek(fin, 0, SEEK_SET);  //same as rewind(fin);

  // Allocate buffer for input file content
  buffer = (char*)malloc(fsize + 1);
  // Read entire file
  fread(buffer, fsize, 1, fin);
  // Close the input stream reader
  fclose(fin);
  verbose("Read %ld bytes.\n", fsize);
  // Set buffer as a 0-terminated string
  buffer[fsize] = 0;



  //
  // Count questions
  //

  // Backup buffer
  buf2count = (char*)malloc(fsize + 1);
  strcpy(buf2count, buffer);

  // Count questions
  char *tmp = buf2count;
  while( (tmp = strstr(tmp, CUEN)) ) {
    ocur_count++;
    tmp++;
  }
  verbose("questions: %d\n", ocur_count);

  // Exit when no-questions found
  if (ocur_count == 0) {
    // Free used memory
    free(buf2count);
    free(buffer);

    // Exit
    return 0;
  }

  if (nvalue > ocur_count) {
    verbose("ERROR: Flag -n value must be less than questions found [%d].\n", ocur_count);

    // Free used memory
    free(buf2count);
    free(buffer);

    return 1;
  }



  //
  // Load questions
  //
  qst_ptrs = (char**)malloc(ocur_count * sizeof(char*));
  tmp = buf2count;
  char* prev = NULL;
  int curr = 0;
  int qst_s;

  while( (tmp = strstr(tmp, CUEN)) ) {
    if (prev == NULL) {
      prev = tmp;

      // Next
      tmp++;
    } else {
      // Compute previous question size (in bytes)
      qst_s = tmp - prev;

      verbose("question: { id: %d, size: %d, start: %p, end: %p }\n",
        curr, qst_s, prev, tmp-1);

      // Allocate memory for question
      qst_ptrs[curr] = (char*)malloc( sizeof(char) * qst_s );

      // Copy question bytes
      strncpy(qst_ptrs[curr], prev, qst_s);

      prev = tmp;

      // Next
      tmp++;

      curr++;
    }
  }
  qst_s = strlen(prev);
  verbose("question: { id: %d, size: %d, start: %p, end: %p }\n",
    curr, qst_s, prev, prev + qst_s);
  qst_ptrs[curr] = (char*)malloc( sizeof(char) * qst_s );
  strncpy(qst_ptrs[curr], prev, qst_s);



  //
  // Question raffle
  //
  qst_raffle = (bool*)calloc(ocur_count, sizeof(char));
  int raffle_counter = 0;
  // Dummy seed for PRNG
  srand(time(NULL));
  while (raffle_counter < nvalue) {
    // Raffle
    int rnd = rand() % ocur_count;

    if (qst_raffle[rnd] == false) {
      // Set question index as elected
      qst_raffle[rnd] = true;
      // Increment raffle counter
      raffle_counter++;
    }
  }
  // Show raffle results
  verbose("raffle: { ");
  for (int i = 0; i < ocur_count - 1; ++i) {
    verbose("%d: %d, ", i, qst_raffle[i]);
  }
  verbose("%d: %d }\n", ocur_count - 1, qst_raffle[ocur_count - 1]);



  //
  // Write question in output file
  //
  fout = fopen(ovalue, "w");
  if (fout == NULL) {
    verbose("ERROR: Cannot open output file [%s].\n", ovalue);

    //
    // Free used memory
    //
    free(qst_raffle);
    for (int i = 0; i < ocur_count; ++i) { free(qst_ptrs[i]); }
    free(qst_ptrs);
    free(buffer);
    free(buf2count);

    return 1;
  }

  // Write questions
  for (int i = 0; i < ocur_count; ++i) {
    if (qst_raffle[i]) {
      fprintf(fout, "%s\n", qst_ptrs[i]);
    }
  }

  // Close output stream writer
  fclose(fout);


  //
  // Free used memory
  //
  free(qst_raffle);
  for (int i = 0; i < ocur_count; ++i) { free(qst_ptrs[i]); }
  free(qst_ptrs);
  free(buffer);
  free(buf2count);


  return 0;

}