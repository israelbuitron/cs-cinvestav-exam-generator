CC=gcc
CFLAGS=-std=c99 -g -Wall

all: gen

gen: log.o
	$(CC) $(CFLAGS) -o gen log.o gen.c

log.o:
	$(CC) $(CFLAGS) -c -o log.o log.c

.PHONY: clean

clean:
	rm -vrf *.o gen *.dSYM

